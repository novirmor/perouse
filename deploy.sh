#!/usr/bin/env bash
for FILE in *.yml
do
	stack_name=$(echo "${FILE%%.*}")
	docker stack deploy --compose-file $FILE $stack_name
done